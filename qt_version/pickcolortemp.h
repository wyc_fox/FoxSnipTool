#ifndef PICKCOLORTEMP_H
#define PICKCOLORTEMP_H

#include <QWidget>
#include <QRect>

class QMouseEvent;
class QKeyEvent;
class QMenu;

class PickColorTemp : public QWidget
{
    Q_OBJECT
public:
    explicit PickColorTemp(QWidget *parent = 0);
    ~PickColorTemp();
    //void Dispose();

protected:
    void showEvent(QShowEvent*e);
    bool eventFilter(QObject *o, QEvent *e);
    void mousePressEvent(QMouseEvent*e);
    void mouseMoveEvent(QMouseEvent*e);
    void mouseReleaseEvent(QMouseEvent*e);
    void paintEvent(QPaintEvent*);
    void contextMenuEvent(QContextMenuEvent *e);
    void keyPressEvent(QKeyEvent* event);

    void createContentMenu();

public slots:
    void ZoomTriggered(QAction *ac);
    void ShapeTriggered(QAction *ac);
    //void leaveEvent(QEvent *e);
private:
    QPoint m_pos;
    QPixmap m_pix;
    QPixmap m_tempPix;
    bool m_isDis;
    qreal m_O, m_width,m_height;
    int _shapeMode;//形状模式
    QMenu       *contentMenu;

    QColor borderCol;
    QColor crossCol;
    QColor fontCol;
    int fontSize;

};

#endif // PICKCOLORTEMP_H
