#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <QWidget>
#include <QShowEvent>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QEnterEvent>
#include <QWheelEvent>

class QPixmap;
class QAction;
class QMenu;
class QPoint;
class QColor;
class QLabel;
class QPoint;
class QSize;


class SnapShot : public QWidget
{
    Q_OBJECT
public:
    explicit SnapShot(QWidget *parent = 0);
    SnapShot(QPixmap& pix, QWidget *parent = 0);
    ~SnapShot();

    void ShowMenu();

signals:

public slots:
    void CloseWidget();     //关闭
    void SavePix();         //保存
    void SetBorderColor();  //设置边框颜色
    void SaveAsPix();       //另存

protected:
    void showEvent(QShowEvent* event);
    void paintEvent(QPaintEvent* event);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void contextMenuEvent(QContextMenuEvent *);

    void keyPressEvent(QKeyEvent* event);
    void enterEvent(QEvent * event);
    void leaveEvent(QEvent * event);
    void wheelEvent(QWheelEvent * event);

private:
    void createActionMenu();
    QPoint redress(QPoint pos);   //矫正最小化 窗口
    QColor RandBorderColor();  //随机边框颜色

private:
    QPixmap     showPix;
    QMenu       *contentMenu;

    QSize       scaleSize;
    float       scaleFactorW;   //缩放因子
    float       scaleFactorH;
    QPoint      mousePressPos;
    QColor      borderColor;

    bool        bMouseMove;
    bool        bMouseInWight;

};

#endif // SNAPSHOT_H
