#include "tipbar.h"
#include "ui_tipbar.h"
#include <QMenu>
#include <QCursor>
#include <QMouseEvent>
#include "appcontroller.h"
#include <QTimerEvent>

TipBar::TipBar(QWidget *parent) :
    QWidget(parent)
  ,  ui(new Ui::TipBar)
  , contentMenu(nullptr)
  , bMouseMove(false)
  , bTaskRunning(false)
  , timerId(-1)
{
    ui->setupUi(this);

    setWindowOpacity(1);
    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);
    setAttribute(Qt::WA_TranslucentBackground);

    createContentMenu();
}

TipBar::~TipBar()
{
    delete ui;
    if(contentMenu)delete contentMenu;
}

void TipBar::ShowMenu()
{
    if(contentMenu){
        QCursor cur=this->cursor();
        contentMenu->exec(cur.pos()); //关联到光标
    }
}

void TipBar::StartTask()
{
    StopTask();
    timerId = this->startTimer(1000);
}

void TipBar::StopTask()
{
    if(timerId != -1){
        this->killTimer(timerId);
    }
    timerId = -1;
}

void TipBar::contextMenuEvent(QContextMenuEvent *)
{
    ShowMenu();
}

void TipBar::mousePressEvent(QMouseEvent *event)
{
    bMouseMove=true;
    mousePressPos = event->globalPos() - this->pos();
}

void TipBar::mouseMoveEvent(QMouseEvent *event)
{
    if(bMouseMove){
        move(event->globalPos() - mousePressPos);
    }
}

void TipBar::mouseReleaseEvent(QMouseEvent *event)
{
    bMouseMove=false;
}

void TipBar::timerEvent(QTimerEvent *event)
{
    if(!this->isHidden() && bTaskRunning &&  timerId == event->timerId()){

    }
}

void TipBar::closeActionCallback()
{
    AppController::GetInstance()->StopTipBarTask();
}

void TipBar::pauseActionCallback()
{

}

void TipBar::setActionCallback()
{

}

void TipBar::createContentMenu()
{
    auto closeAct = new QAction(tr("停止"), this);
    closeAct->setIcon(QIcon(":/icon/res/drop_icon.png"));
    connect(closeAct, SIGNAL(triggered()), this, SLOT(closeActionCallback()));
    auto pauseAct = new QAction(tr("暂停5分钟"), this);
    pauseAct->setIcon(QIcon(":/icon/res/quicksave_icon.png"));
    connect(pauseAct, SIGNAL(triggered()), this, SLOT(pauseActionCallback()));
    auto setAct = new QAction(tr("设置"), this);
    setAct->setIcon(QIcon(":/icon/res/border_icon.png"));
    connect(setAct, SIGNAL(triggered()), this, SLOT(setActionCallback()));

    contentMenu=new QMenu(this);
    contentMenu->addAction(setAct);
    contentMenu->addSeparator();
    contentMenu->addAction(pauseAct);
    contentMenu->addAction(closeAct);

}
