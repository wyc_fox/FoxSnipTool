#ifndef LOG_H
#define LOG_H

#include <QDebug>

#define Log qDebug()<<"[Log]"<<__FILE__<<"("<<__LINE__<<")"<<__FUNCTION__

#endif // LOG_H
