﻿#include "globalmgr.h"
#include <QSettings>
#include "log.h"
#include <QFile>
#include <QDir>
#include <QMessageBox>
#include <QStandardPaths>
#include <QObject>

GlobalMgr* GlobalMgr::mIns = nullptr;

GlobalMgr::GlobalMgr()
{
    bInit = loadConfig();
}

bool GlobalMgr::loadConfig()
{
    if(!QFile::exists(GetConfigPath())){
        QMessageBox::information(nullptr,QObject::tr("提示"),QObject::tr("配置文件丢失,创建默认配置!"));
        createDefaultConfig();
    }else{
        Log<<"loadConfig";

        QSettings *configIniRead = new QSettings(GetConfigPath(), QSettings::IniFormat);
        if(!configIniRead){
            Log<<"QSetting error";
            return false;
        }

        SetQuickSavePath(configIniRead->value("/SnapShot/quickSavePath").toString());
        SetSaveTypeByInt(configIniRead->value("/SnapShot/imgSaveType").toInt());
        SetAutoStart(configIniRead->value("/SnapShot/bAutoStart").toBool());
        SetUseClipBoard(configIniRead->value("/SnapShot/bUseClipBoard").toBool());
        SetBorderWidth(configIniRead->value("/SnapShot/borderWidth").toInt());


        SetPickShapeTypeByInt(configIniRead->value("PickColor/shape").toInt());
        SetPickBaseSize(configIniRead->value("PickColor/baseSize").toSize());
        SetPickZoom(configIniRead->value("PickColor/zoom").toInt());
        SetPickRGBFontSize(configIniRead->value("PickColor/fontSize").toInt());
        SetPickBorderColor(configIniRead->value("PickColor/borderColor").value<QColor>());
        SetPickCrossColor(configIniRead->value("PickColor/crossColor").value<QColor>());
        SetPickRGBColor(configIniRead->value("PickColor/rgbColor").value<QColor>());

        delete configIniRead;
    }

    return true;
}

bool GlobalMgr::saveConfig()
{
    Log<<"saveConfig";

    QSettings *configIniRead = new QSettings(GetConfigPath(), QSettings::IniFormat);
    if(!configIniRead){
        Log<<"QSetting error";
        return false;
    }

    // snap
    configIniRead->setValue("SnapShot/quickSavePath"
                            ,snapCfgData.quickSavePath);
    configIniRead->setValue("SnapShot/imgSaveType"
                            ,snapCfgData.imgSaveType);
    configIniRead->setValue("SnapShot/bAutoStart"
                            ,snapCfgData.bAutoStart);
    configIniRead->setValue("SnapShot/bUseClipBoard"
                            ,snapCfgData.bUseClipBoard);
    configIniRead->setValue("SnapShot/borderWidth"
                            ,snapCfgData.borderWidth);


    // pick
    configIniRead->setValue("PickColor/shape"
                            ,pickCfgData.shape);
    configIniRead->setValue("PickColor/zoom"
                            ,pickCfgData.zoom);
    configIniRead->setValue("PickColor/fontSize"
                            ,pickCfgData.fontSize);
    configIniRead->setValue("PickColor/borderColor"
                            ,pickCfgData.borderColor);
    configIniRead->setValue("PickColor/crossColor"
                            ,pickCfgData.crossColor);
    configIniRead->setValue("PickColor/rgbColor"
                            ,pickCfgData.rgbColor);
    configIniRead->setValue("PickColor/baseSize"
                            ,pickCfgData.baseSize);


    delete configIniRead;
    return true;
}

QString GlobalMgr::GetDesktopPath()
{
    return QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).first();
}

void GlobalMgr::createDefaultConfig()
{
    Log<<"createDefaultConfig";

    QSettings *configIniRead = new QSettings(GetConfigPath(), QSettings::IniFormat);
    if(!configIniRead){
        Log<<"QSetting error";
        return ;
    }

    // snap
    snapCfgData.quickSavePath = QStandardPaths::standardLocations(QStandardPaths::DesktopLocation).first();
    snapCfgData.imgSaveType = SaveType::Type_png;
    snapCfgData.bAutoStart = false;
    snapCfgData.bUseClipBoard = true;
    snapCfgData.borderWidth =1;
    configIniRead->setValue("SnapShot/quickSavePath"
                            ,snapCfgData.quickSavePath);
    configIniRead->setValue("SnapShot/imgSaveType"
                            ,snapCfgData.imgSaveType);
    configIniRead->setValue("SnapShot/bAutoStart"
                            ,snapCfgData.bAutoStart);
    configIniRead->setValue("SnapShot/bUseClipBoard"
                            ,snapCfgData.bUseClipBoard);
    configIniRead->setValue("SnapShot/borderWidth"
                            ,snapCfgData.borderWidth);

    // pick
    configIniRead->setValue("PickColor/shape"
                            ,pickCfgData.shape);
    configIniRead->setValue("PickColor/zoom"
                            ,pickCfgData.zoom);
    configIniRead->setValue("PickColor/fontSize"
                            ,pickCfgData.fontSize);
    configIniRead->setValue("PickColor/borderColor"
                            ,pickCfgData.borderColor);
    configIniRead->setValue("PickColor/crossColor"
                            ,pickCfgData.crossColor);
    configIniRead->setValue("PickColor/rgbColor"
                            ,pickCfgData.rgbColor);
    configIniRead->setValue("PickColor/baseSize"
                            ,pickCfgData.baseSize);


    delete configIniRead;
}

GlobalMgr *GlobalMgr::GetInstance()
{
    if(GlobalMgr::mIns == nullptr)
    {
        mIns = new GlobalMgr;
    }
    return GlobalMgr::mIns;
}

QString GlobalMgr::GetConfigPath()
{
    return QDir::currentPath() + "/Config.ini";
}
