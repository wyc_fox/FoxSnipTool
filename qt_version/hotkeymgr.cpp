#include "hotkeymgr.h"
#include "windows.h"
#include <QByteArray>
#include "log.h"
#include "snapshottemp.h"
#include "appcontroller.h"

int HotKey = 1008611;
int HotKey2 = 1008622;

HotKeyMgr::HotKeyMgr()
{
    RegisterHotKey(0,HotKey,MOD_CONTROL,Qt::Key_1);
    RegisterHotKey(0,HotKey2,MOD_CONTROL,Qt::Key_2);
}

HotKeyMgr::~HotKeyMgr()
{
    UnregisterHotKey(0,HotKey);
    UnregisterHotKey(0,HotKey2);
}

bool HotKeyMgr::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
{
    if(eventType == "windows_generic_MSG")
    {
        MSG *msg = static_cast<MSG*>(message);
        if(msg->message == WM_HOTKEY)
        {
            const quint32 keycode = HIWORD(msg->lParam);
            const quint32 modifiers = LOWORD(msg->lParam);

            if(keycode == '1' && modifiers == MOD_CONTROL)
            {
                Log<<"ctrl + 1";
                AppController::GetInstance()->CreateSnapShotTemp();
                return true;
            }
            if(keycode == '2' && modifiers == MOD_CONTROL)
            {
                Log<<"ctrl + 2";
                AppController::GetInstance()->CreatePickColorTemp();
                return true;
            }
        }
    }
    return false;
}
