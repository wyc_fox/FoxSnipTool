#-------------------------------------------------
#
# Project created by QtCreator 2017-01-07T17:04:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FoxTools
TEMPLATE = app


SOURCES += main.cpp\
        settingwindow.cpp \
    customslider.cpp \
    globalmgr.cpp \
    hotkeymgr.cpp \
    snapshottemp.cpp \
    appcontroller.cpp \
    snapshot.cpp \
    pickcolortemp.cpp \
    colorlabel.cpp \
    tipbar.cpp

HEADERS  += settingwindow.h \
    customslider.h \
    globalmgr.h \
    hotkeymgr.h \
    snapshottemp.h \
    appcontroller.h \
    snapshot.h \
    pickcolortemp.h \
    log.h \
    colorlabel.h \
    tipbar.h

FORMS    += settingwindow.ui \
    tipbar.ui

RESOURCES += \
    res.qrc

RC_FILE += exeicon.rc
