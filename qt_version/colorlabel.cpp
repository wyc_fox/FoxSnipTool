#include "colorlabel.h"
#include <QMouseEvent>
#include <QColorDialog>
#include <QColor>

ColorLabel::ColorLabel(QWidget* parent)
    :QLabel(parent)
    ,curColor(QColor(255,0,0))
{
    QPixmap fillpix(this->geometry().width(),this->geometry().height());
    fillpix.fill(curColor);
    this->setPixmap(fillpix);
}

void ColorLabel::SetColor(QColor col)
{
    QPixmap fillpix(this->geometry().width(),this->geometry().height());
    fillpix.fill(col);
    this->setPixmap(fillpix);
    curColor = col;
    emit colorChange(curColor);
    this->update();
}

void ColorLabel::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        SetColor(QColorDialog::getColor(Qt::white, this));
    }

    QLabel::mousePressEvent(event);//将该事件传给父类处理
}
