#ifndef TIPBAR_H
#define TIPBAR_H

#include <QWidget>

class QMenu;
class QPoint;

namespace Ui {
class TipBar;
}

class TipBar : public QWidget
{
    Q_OBJECT

public:
    explicit TipBar(QWidget *parent = 0);
    ~TipBar();

    void ShowMenu();
    void StartTask();
    void StopTask();

protected:
    void contextMenuEvent(QContextMenuEvent *);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void timerEvent( QTimerEvent *event );

private slots:
    void closeActionCallback();
    void pauseActionCallback();
    void setActionCallback();

private:
    void createContentMenu();

private:
    Ui::TipBar *ui;
    QMenu* contentMenu;
    QPoint      mousePressPos;

    int timerId;

    bool        bMouseMove;
    bool        bTaskRunning;
};

#endif // TIPBAR_H
