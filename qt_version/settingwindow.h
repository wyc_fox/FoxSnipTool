#ifndef SETTINGWINDOW_H
#define SETTINGWINDOW_H

#include <QMainWindow>
#include <QShowEvent>
#include <QHideEvent>
#include <QSystemTrayIcon>

class QAction;
class QMenu;
class QCheckBox;
class QButtonGroup;

namespace Ui {
class SettingWindow;
}

class SettingWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SettingWindow(QWidget *parent = 0);
    ~SettingWindow();

private slots:
    //void trayiconActivated(QSystemTrayIcon::ActivationReason reason);
    //void showSettingWindow();
//    void clearAllSnap();
//    void showAllSnap();
//    void hideAllSnap();
//    void helpCallback();

    // snap
    void savePathLineEditCallback();
    void scanBtnCallback();
    void defaultBtnCallback();
    void useClipCallback(int state);
    void autoStartCallback(int state);
    void saveTypeCallback();

    //pick color
    void borderShapeTypeCallback();
    void zoomCallback();
    void borderColorCallback(QColor col);
    void crossColorCallback(QColor col);
    void rgbFontColorCallback(QColor col);

    // tips
    void startBtnCallback();

protected:
    void showEvent(QShowEvent *event);
    void hideEvent(QHideEvent *event);

    void updateSettingWindow();

private:
    //void createContentMenu();
    void initSnapConnect();
    void initPickColorConnect();
    void initTipsConnect();


private:
    Ui::SettingWindow *ui;

    //QSystemTrayIcon     *trayIcon;
   // QMenu               *trayMenu;

    QButtonGroup*     group;
    QButtonGroup*     group2;
    QButtonGroup*     group3;

};

#endif // SETTINGWINDOW_H
