#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <iostream>
#include <vector>
#include <map>
#include <QObject>
#include <QSystemTrayIcon>

class QWidget;
class SnapShotTemp;
class PickColorTemp;
class QApplication;
class TipBar;
class SettingWindow;
class HotKeyMgr;
class QMenu;

class AppController : public QObject
{
    Q_OBJECT
private:
    explicit  AppController(QObject *parent = 0);
    ~AppController();

public:
    static AppController *GetInstance();

    void SetApplication(QApplication* a){ mApp = a;}
    void RegisterHotKeyEvent(QApplication* a);

    void ShowSettingWindow();
    void HideSettingWindow();

    void AddSnapShot(QWidget* wgt);
    void RemoveSnapShot(QWidget* wgt);
    void RemoveActivitySnapShot();
    void RemoveAllSnapShot();
    void ShowAllSnapShot();
    void HideAllSnapShot();

    void CreateSnapShotTemp();
    void CreatePickColorTemp();
    void DeadWithSnapShot();
    void DeadWithPickColor();

    void StartTipBarTask();
    void StopTipBarTask();

private slots:
    void trayiconActivated(QSystemTrayIcon::ActivationReason reason);

    void setActionCallback();
    void clearAllSnap();
    void showAllSnap();
    void hideAllSnap();
    void helpCallback();

private:
    void init();
    void createContentMenu();

private:
    static AppController* mIns;
    QApplication*   mApp;
    HotKeyMgr* hotKeyMgr;
    QSystemTrayIcon* trayIcon;
    QMenu* trayMenu;

    SettingWindow* mSettingWindow;
    std::vector< QWidget* > mSnapShotVec;
    SnapShotTemp* mSnapShotTemp;
    PickColorTemp* mPickColorTemp;
    TipBar* mTipBar;

};

#endif // APPCONTROLLER_H
