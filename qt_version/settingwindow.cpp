#include "settingwindow.h"
#include "ui_settingwindow.h"
#include <QApplication>
#include <QDir>
#include <QFileDialog>
#include <QButtonGroup>
#include <QColorDialog>
#include "appcontroller.h"
#include "globalmgr.h"
#include "Log.h"

SettingWindow::SettingWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SettingWindow)
{
    ui->setupUi(this);

    // system tray icon
//    trayIcon = new QSystemTrayIcon(QIcon(":/icon/res/tray_icon.png"),this);
//    trayIcon->setToolTip(tr("FoxTools"));
//    QString titlec=tr("Title");
//    QString textc=tr("[Ctrl+1]截图\n[Ctrl+2]取色");
//    trayIcon->show();
//    trayIcon->showMessage(titlec,textc,QSystemTrayIcon::Information,5000);
//    connect(trayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
//                this,SLOT(trayiconActivated(QSystemTrayIcon::ActivationReason)));


    //createContentMenu();
    initSnapConnect();
    initPickColorConnect();
    initTipsConnect();

    this->setWindowIcon(QIcon(":/icon/res/tray_icon.png"));

    //QApplication::setQuitOnLastWindowClosed(false);
}

SettingWindow::~SettingWindow()
{
    //if(trayIcon)delete trayIcon;
    delete ui;
}

//void SettingWindow::trayiconActivated(QSystemTrayIcon::ActivationReason reason)
//{
//    switch (reason){
//    case QSystemTrayIcon::Trigger:
//        // one clicked tray icon
//        break;
//    case QSystemTrayIcon::DoubleClick:
//        // double clicked tray icon
//        showSettingWindow();
//        break;
//    default:
//        break;
//    }
//}

//void SettingWindow::showSettingWindow()
//{
//   this->showNormal();
//   this->raise();
//    activateWindow();
//}

//void SettingWindow::clearAllSnap()
//{
//    AppController::GetInstance()->RemoveAllSnapShot();
//}

//void SettingWindow::showAllSnap()
//{
//   AppController::GetInstance()->ShowAllSnapShot();
//}

//void SettingWindow::hideAllSnap()
//{
//    AppController::GetInstance()->HideAllSnapShot();
//}

//void SettingWindow::helpCallback()
//{

//}

void SettingWindow::savePathLineEditCallback()
{
    GlobalMgr::GetInstance()->SetQuickSavePath(ui->quickSavePath->text());
}

void SettingWindow::scanBtnCallback()
{
    QString dirPath = QFileDialog::getExistingDirectory();
    if(!dirPath.isEmpty()){
        ui->quickSavePath->setText(dirPath);
        savePathLineEditCallback();
    }
}

void SettingWindow::defaultBtnCallback()
{
    ui->quickSavePath->setText(GlobalMgr::GetInstance()->GetDesktopPath());
}

void SettingWindow::useClipCallback(int state)
{
    GlobalMgr::GetInstance()->SetUseClipBoard(state);
}

void SettingWindow::autoStartCallback(int state)
{
    GlobalMgr::GetInstance()->SetAutoStart(state);
}

void SettingWindow::saveTypeCallback()
{
    GlobalMgr::GetInstance()->SetSaveTypeByInt(group->checkedId());
}

void SettingWindow::borderShapeTypeCallback()
{
    int type = group2->checkedId();

    Log<<"shape :"<<type;
}

void SettingWindow::zoomCallback()
{
    int zoom = group3->checkedId();
    Log<<"zoom :"<<zoom;
}

void SettingWindow::borderColorCallback(QColor col)
{
    GlobalMgr::GetInstance()->SetPickBorderColor(col);
}

void SettingWindow::crossColorCallback(QColor col)
{
    GlobalMgr::GetInstance()->SetPickCrossColor(col);
}

void SettingWindow::rgbFontColorCallback(QColor col)
{
    GlobalMgr::GetInstance()->SetPickRGBColor(col);
}

void SettingWindow::startBtnCallback()
{
    AppController::GetInstance()->StartTipBarTask();
}

void SettingWindow::hideEvent(QHideEvent *event)
{
    Log<<"hide window";
    GlobalMgr::GetInstance()->saveConfig();
}

void SettingWindow::updateSettingWindow()
{
    GlobalMgr* mgr = GlobalMgr::GetInstance();

    // snap
    ui->quickSavePath->setText(mgr->GetQuickSavePath());
    ui->autoStart->setChecked(mgr->GetAutoStart());
    auto type = mgr->GetSaveType();
    switch (type) {
    case SaveType::Type_bmp:
        ui->bmpRadioBtn->setChecked(true);
        break;
    case SaveType::Type_jpg:
        ui->jpgRadioBtn->setChecked(true);
        break;
    case SaveType::Type_png:
        ui->pngRadioBtn->setChecked(true);
        break;
    }

    ui->useClipBoard->setChecked(mgr->GetUseClipBoard());

    // pick color
    auto shape = mgr->GetPickShapeType();
    switch(shape)
    {
    case ShapeType::Shape_circle:
        ui->circleRadio->setChecked(true);
        break;
    case ShapeType::Shape_rectangle:
        ui->rectangleRadio->setChecked(true);
        break;
    }
    int zoom = mgr->GetPickZoom();
    switch(zoom){
    case 100:
        ui->zoom100Radio->setChecked(true);
        break;
    case 150:
        ui->zoom150Radio->setChecked(true);
        break;
    case 200:
        ui->zoom200Radio->setChecked(true);
        break;
    case 400:
        ui->zoom400Radio->setChecked(true);
        break;
    default:
        //ui->zoom150Radio->setChecked(true);
        break;
    }
    ui->boderColorLabel->SetColor(mgr->GetPickBorderColor());
    ui->crossColorLabel->SetColor(mgr->GetPickCrossColor());
    ui->rgbColorLabel->SetColor(mgr->GetPickRGBColor());
}

//void SettingWindow::createContentMenu()
//{
//    // create right menu
//    auto setAction = new QAction(tr("设置"), this);
//    setAction->setIcon(QIcon(":/icon/res/setting_icon.png"));
//    connect(setAction, SIGNAL(triggered()), this , SLOT(showSettingWindow()));
//    auto quitAction = new QAction(tr("退出"), this);
//    quitAction->setIcon(QIcon(":/icon/res/power_exit_icon.png"));
//    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
//    auto helpAct = new QAction(tr("帮助"),this);
//    helpAct->setIcon(QIcon(":/icon/res/help_icon.png"));
//    connect(helpAct, SIGNAL(triggered()), this , SLOT(helpCallback()));
//    auto clearAllAct = new QAction(tr("全部清除"),this);
//    clearAllAct->setIcon(QIcon(":/icon/res/clearall_icon.png"));
//    connect(clearAllAct, SIGNAL(triggered()), this , SLOT(clearAllSnap()));
//    auto showAllAct = new QAction(tr("全部显示"),this);
//    connect(showAllAct, SIGNAL(triggered()), this , SLOT(showAllSnap()));
//    auto hideAllAct = new QAction(tr("全部隐藏"),this);
//    connect(hideAllAct, SIGNAL(triggered()), this , SLOT(hideAllSnap()));



//    trayMenu = new QMenu(this);
//    trayMenu->addAction(setAction);
//    trayMenu->addSeparator();
//    trayMenu->addAction(showAllAct);
//    trayMenu->addAction(hideAllAct);
//    trayMenu->addAction(clearAllAct);
//    trayMenu->addSeparator();
//    trayMenu->addAction(helpAct);
//    trayMenu->addAction(quitAction);
//    trayIcon->setContextMenu(trayMenu);
//}

void SettingWindow::initSnapConnect()
{
    // connect callback
    connect(ui->quickSavePath,SIGNAL(editingFinished()),this,SLOT(savePathLineEditCallback()));
    connect(ui->scanBtn,SIGNAL(released()),this,SLOT(scanBtnCallback()));
    connect(ui->desktopBtn,SIGNAL(released()),this,SLOT(defaultBtnCallback()));
    connect(ui->useClipBoard , SIGNAL(stateChanged(int)),this ,SLOT(useClipCallback(int)));
    connect(ui->autoStart , SIGNAL(stateChanged(int)),this ,SLOT(autoStartCallback(int)));


    group= new QButtonGroup(this);
    group->addButton(ui->bmpRadioBtn, SaveType::Type_bmp);
    group->addButton(ui->jpgRadioBtn, SaveType::Type_jpg);
    group->addButton(ui->pngRadioBtn, SaveType::Type_png);
    connect(ui->bmpRadioBtn, SIGNAL(clicked()), this, SLOT(saveTypeCallback()));
    connect(ui->jpgRadioBtn, SIGNAL(clicked()), this, SLOT(saveTypeCallback()));
    connect(ui->pngRadioBtn, SIGNAL(clicked()), this, SLOT(saveTypeCallback()));
}

void SettingWindow::initPickColorConnect()
{
    // pick shape
    group2= new QButtonGroup(this);
    group2->addButton(ui->circleRadio, 1);
    group2->addButton(ui->rectangleRadio, 2);
    connect(ui->circleRadio, SIGNAL(clicked()), this, SLOT(borderShapeTypeCallback()));
    connect(ui->rectangleRadio, SIGNAL(clicked()), this, SLOT(borderShapeTypeCallback()));

    // pick zoom factor
    group3= new QButtonGroup(this);
    group3->addButton(ui->zoom100Radio, 100);
    group3->addButton(ui->zoom150Radio, 150);
    group3->addButton(ui->zoom200Radio, 200);
    group3->addButton(ui->zoom400Radio, 400);
    connect(ui->zoom100Radio, SIGNAL(clicked()), this, SLOT(zoomCallback()));
    connect(ui->zoom150Radio, SIGNAL(clicked()), this, SLOT(zoomCallback()));
    connect(ui->zoom200Radio, SIGNAL(clicked()), this, SLOT(zoomCallback()));
    connect(ui->zoom400Radio, SIGNAL(clicked()), this, SLOT(zoomCallback()));


    connect(ui->boderColorLabel, SIGNAL(colorChange(QColor)), this, SLOT(borderColorCallback(QColor)));
    connect(ui->crossColorLabel, SIGNAL(colorChange(QColor)), this, SLOT(crossColorCallback(QColor)));
    connect(ui->rgbColorLabel, SIGNAL(colorChange(QColor)), this, SLOT(rgbFontColorCallback(QColor)));


}

void SettingWindow::initTipsConnect()
{
    connect(ui->startBtn,SIGNAL(released()),this,SLOT(startBtnCallback()));
}

void SettingWindow::showEvent(QShowEvent *event)
{
    Log<<"show window";
    updateSettingWindow();
}





























