#ifndef HOTKEYMGR_H
#define HOTKEYMGR_H

#include <QAbstractNativeEventFilter>

class HotKeyMgr : public QAbstractNativeEventFilter
{
public:
    HotKeyMgr();
    ~HotKeyMgr();
    bool nativeEventFilter(const QByteArray &eventType, void *message, long *result);
};

#endif // HOTKEYMGR_H
