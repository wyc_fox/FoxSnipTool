#ifndef COLORLABEL_H
#define COLORLABEL_H

#include <QLabel>

class QMouseEvent;
class QColor;

class ColorLabel : public QLabel
{
    Q_OBJECT
public:
    ColorLabel(QWidget* parent = 0);

    QColor GetColor(){return curColor;}
    void SetColor(QColor col);

signals:
    void colorChange(QColor col);
protected:
    void mousePressEvent(QMouseEvent* event);

private:
    QColor  curColor;

};

#endif // COLORLABEL_H
