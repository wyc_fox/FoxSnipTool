#ifndef SNAPSHOTTEMP_H
#define SNAPSHOTTEMP_H

#include <QWidget>

class QPixmap;
class QRect;
class QShowEvent;
class QPaintEvent;
class QMouseEvent;
class QKeyEvent;

class SnapShotTemp : public QWidget
{
    Q_OBJECT
public:
    explicit SnapShotTemp(QWidget *parent = 0);

    void Dispose();

protected:
    void showEvent(QShowEvent* event);
    void paintEvent(QPaintEvent* event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent  *event);


signals:

public slots:

private:
    QPixmap fullScreenPix;
    QRect   cutRect;

    bool    bFinish;

};

#endif // SNAPSHOTTEMP_H
