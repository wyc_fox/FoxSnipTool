#include "appcontroller.h"
#include <QWidget>
#include <QApplication>
#include "snapshot.h"
#include "snapshottemp.h"
#include "pickcolortemp.h"
#include "tipbar.h"
#include "settingwindow.h"
#include "hotkeymgr.h"
#include "globalmgr.h"
#include <QMenu>


AppController* AppController::mIns = nullptr;

AppController::AppController(QObject *parent)
    :QObject(parent)
{
    mSnapShotTemp = nullptr;
    mPickColorTemp = nullptr;
    mApp = nullptr;
    mTipBar = nullptr;
    mSettingWindow = nullptr;
    hotKeyMgr = nullptr;
    trayIcon = nullptr;
}

AppController::~AppController()
{
    for(auto iter = mSnapShotVec.begin();iter!=mSnapShotVec.end();++iter){
        delete *iter;
    }
    if(!mSnapShotTemp) delete mSnapShotTemp;
    if(!mPickColorTemp) delete mPickColorTemp;
    if(!mSettingWindow) delete mSettingWindow;
    if(!hotKeyMgr) delete hotKeyMgr;
    if(!trayIcon) delete trayIcon;
}

AppController *AppController::GetInstance()
{
    if(!mIns){
        mIns = new AppController();
        mIns->init();
    }
    return mIns;
}

void AppController::RegisterHotKeyEvent(QApplication *a)
{
    mApp = a;
    hotKeyMgr = new HotKeyMgr();
    a->installNativeEventFilter(hotKeyMgr);
}

void AppController::ShowSettingWindow()
{
    if(mSettingWindow)mSettingWindow->show();
}

void AppController::HideSettingWindow()
{
    if(mSettingWindow)mSettingWindow->hide();
}

void AppController::AddSnapShot(QWidget *wgt)
{
    if(!wgt)return;
    mSnapShotVec.push_back(wgt);
}

void AppController::RemoveSnapShot(QWidget *wgt)
{
    if(!wgt)return;
    for(auto iter = mSnapShotVec.begin();iter!=mSnapShotVec.end();++iter){
        QWidget* temp = static_cast<QWidget*>(*iter);
        if(temp && (temp == wgt)){
            mSnapShotVec.erase(iter);
            temp->close();
            return;
        }
    }
}

void AppController::RemoveActivitySnapShot()
{
    for(auto iter = mSnapShotVec.begin();iter!=mSnapShotVec.end();++iter){
        SnapShot* temp = static_cast< SnapShot* >(*iter);
        if(temp && temp->isActiveWindow()){
            temp->CloseWidget();
            return;
        }
    }
}

void AppController::RemoveAllSnapShot()
{
    for(auto iter = mSnapShotVec.begin()
        ;iter!=mSnapShotVec.end()
        ;++iter)
    {
        SnapShot* temp = static_cast< SnapShot* >(*iter);
        if(temp)
            temp->close();
    }
    mSnapShotVec.clear();
}

void AppController::ShowAllSnapShot()
{
    for(auto iter = mSnapShotVec.begin()
        ;iter!=mSnapShotVec.end()
        ;++iter)
    {
        SnapShot* temp = static_cast< SnapShot* >(*iter);
        if(temp)
            temp->show();
    }
}

void AppController::HideAllSnapShot()
{
    for(auto iter = mSnapShotVec.begin()
        ;iter!=mSnapShotVec.end()
        ;++iter)
    {
        SnapShot* temp = static_cast< SnapShot* >(*iter);
        if(temp)
        temp->hide();
    }
}

void AppController::CreateSnapShotTemp()
{
    if(mSnapShotTemp)
        delete mSnapShotTemp;

    mSnapShotTemp = new SnapShotTemp();
}

void AppController::CreatePickColorTemp()
{
    if(mPickColorTemp)
        delete mPickColorTemp;
    mPickColorTemp = new PickColorTemp();
    mPickColorTemp->show();
}

void AppController::DeadWithSnapShot()
{
    if(mSnapShotTemp){
        mSnapShotTemp->Dispose();
        delete mSnapShotTemp;
        mSnapShotTemp = nullptr;
    }
}

void AppController::DeadWithPickColor()
{
    if(mPickColorTemp){
        delete mPickColorTemp;
        mPickColorTemp = nullptr;
    }
}

void AppController::StartTipBarTask()
{
    if(mTipBar){
        delete mTipBar;
    }
    mTipBar = new TipBar();
    mTipBar->show();
    mTipBar->StartTask();
}

void AppController::StopTipBarTask()
{
    if(mTipBar){
        mTipBar->StopTask();
        mTipBar->close();
    }
}

void AppController::trayiconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason){
    case QSystemTrayIcon::Trigger:
        // one clicked tray icon
        break;
    case QSystemTrayIcon::DoubleClick:
        // double clicked tray icon
        ShowSettingWindow();
        break;
    default:
        break;
    }
}

void AppController::setActionCallback()
{
    ShowSettingWindow();
}

void AppController::clearAllSnap()
{
    RemoveAllSnapShot();
}

void AppController::showAllSnap()
{
   ShowAllSnapShot();
}

void AppController::hideAllSnap()
{
    HideAllSnapShot();
}

void AppController::helpCallback()
{

}

void AppController::init()
{
    GlobalMgr::GetInstance();
    mSettingWindow = new SettingWindow();

    // system tray icon
    trayIcon = new QSystemTrayIcon(QIcon(":/icon/res/tray_icon.png"),this);
    trayIcon->setToolTip(tr("FoxTools"));
    QString titlec=tr("Title");
    QString textc=tr("[Ctrl+1]截图\n[Ctrl+2]取色");
    trayIcon->show();
    trayIcon->showMessage(titlec,textc,QSystemTrayIcon::Information,5000);
    connect(trayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
                this,SLOT(trayiconActivated(QSystemTrayIcon::ActivationReason)));

    createContentMenu();

    QApplication::setQuitOnLastWindowClosed(false);
}

void AppController::createContentMenu()
{
    // create right menu
    auto setAction = new QAction(tr("设置"), this);
    setAction->setIcon(QIcon(":/icon/res/setting_icon.png"));
    connect(setAction, SIGNAL(triggered()), this , SLOT(setActionCallback()));
    auto quitAction = new QAction(tr("退出"), this);
    quitAction->setIcon(QIcon(":/icon/res/power_exit_icon.png"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    auto helpAct = new QAction(tr("帮助"),this);
    helpAct->setIcon(QIcon(":/icon/res/help_icon.png"));
    connect(helpAct, SIGNAL(triggered()), this , SLOT(helpCallback()));
    auto clearAllAct = new QAction(tr("全部清除"),this);
    clearAllAct->setIcon(QIcon(":/icon/res/clearall_icon.png"));
    connect(clearAllAct, SIGNAL(triggered()), this , SLOT(clearAllSnap()));
    auto showAllAct = new QAction(tr("全部显示"),this);
    connect(showAllAct, SIGNAL(triggered()), this , SLOT(showAllSnap()));
    auto hideAllAct = new QAction(tr("全部隐藏"),this);
    connect(hideAllAct, SIGNAL(triggered()), this , SLOT(hideAllSnap()));



    trayMenu = new QMenu();
    trayMenu->addAction(setAction);
    trayMenu->addSeparator();
    trayMenu->addAction(showAllAct);
    trayMenu->addAction(hideAllAct);
    trayMenu->addAction(clearAllAct);
    trayMenu->addSeparator();
    trayMenu->addAction(helpAct);
    trayMenu->addAction(quitAction);
    trayIcon->setContextMenu(trayMenu);
}
