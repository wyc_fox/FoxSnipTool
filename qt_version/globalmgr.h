﻿#ifndef GLOBALMGR_H
#define GLOBALMGR_H

#include <iostream>
#include <QString>
#include <QSize>
#include <QColor>

enum SaveType
{
    Type_vaild = 0,
    Type_png,
    Type_jpg,
    Type_bmp,
};

enum ShapeType
{
    Shape_circle = 1,
    Shape_rectangle,
};

struct SnapConfigData
{
    QString quickSavePath;
    SaveType imgSaveType;
    bool bAutoStart;
    bool bUseClipBoard;
    int borderWidth;
    QSize miniSize;

    SnapConfigData(){
        quickSavePath = "";
        imgSaveType = SaveType::Type_png;
        bAutoStart = false;
        bUseClipBoard = false;
        borderWidth = 1;
        miniSize = QSize(50,50);
    }
};

struct PickColorConfigData
{
    ShapeType shape;
    QSize baseSize;
    int zoom;
    int fontSize;
    QColor borderColor;
    QColor crossColor;
    QColor rgbColor;

    PickColorConfigData(){
        shape = ShapeType::Shape_rectangle;
        baseSize = QSize(150,150);
        zoom = 150;
        fontSize = 20;
        borderColor = QColor(255,255,0);
        crossColor = QColor(255,0,0);
        rgbColor = QColor(0,0,255);
    }
};


class GlobalMgr
{
private:
    GlobalMgr();
    bool loadConfig();
    void createDefaultConfig();

public:
    static GlobalMgr* GetInstance();
    QString GetConfigPath();
    bool saveConfig();

    /*
     *     snap
     * */
    void SetAutoStart(bool isAuto){
        snapCfgData.bAutoStart = isAuto;
    }
    bool GetAutoStart(){return snapCfgData.bAutoStart;}

    void SetUseClipBoard(bool isUse){
        snapCfgData.bUseClipBoard = isUse;
    }
    bool GetUseClipBoard(){return snapCfgData.bUseClipBoard;}

    void SetQuickSavePath(QString path){
        snapCfgData.quickSavePath = path;
    }
    QString GetQuickSavePath(){return snapCfgData.quickSavePath;}

    void SetBorderWidth(int px){
        snapCfgData.borderWidth = px;
    }
    int GetBorderWidth(){return snapCfgData.borderWidth;}

    void SetSaveType(SaveType type){
        snapCfgData.imgSaveType = type;
    }
    void SetSaveTypeByInt(int type){
        switch (type) {
        case 0:
            snapCfgData.imgSaveType = SaveType::Type_vaild;
            break;
        case 1:
            snapCfgData.imgSaveType = SaveType::Type_png;
            break;
        case 2:
            snapCfgData.imgSaveType = SaveType::Type_jpg;
            break;
        case 3:
            snapCfgData.imgSaveType = SaveType::Type_bmp;
            break;
        }
    }
    SaveType GetSaveType(){return snapCfgData.imgSaveType;}
    QString GetSaveTypeToString(){
        QString temp;
        switch(snapCfgData.imgSaveType)
        {
        case SaveType::Type_bmp:
            temp = ".bmp";
            break;
        case SaveType::Type_jpg:
            temp = ".jpg";
            break;
        case SaveType::Type_png:
            temp = ".png";
            break;
        }
        return temp;
    }

    void SetMiniSize(int w,int h){snapCfgData.miniSize = QSize(w,h);}
    QSize GetMiniSize(){return snapCfgData.miniSize;}

    SnapConfigData GetSnapConfigData(){return snapCfgData;}

    /*
     *  pick color
     * */
    void SetPickShapeType(ShapeType tp){pickCfgData.shape = tp;}
    void SetPickShapeTypeByInt(int tp){
        switch (tp) {
        case 1:
            pickCfgData.shape = ShapeType::Shape_circle;
            break;
        case 2:
            pickCfgData.shape = ShapeType::Shape_rectangle;
            break;
        }
    }

    ShapeType GetPickShapeType(){return pickCfgData.shape;}

    void SetPickBaseSize(QSize sz){pickCfgData.baseSize = sz;}
    QSize GetPickBaseSize(){return pickCfgData.baseSize;}

    void SetPickZoom(int zm){pickCfgData.zoom = zm;}
    int GetPickZoom(){return pickCfgData.zoom;}

    void SetPickRGBFontSize(int sz){pickCfgData.fontSize = sz;}
    int GetPickRGBFontSize(){return pickCfgData.fontSize;}

    void SetPickBorderColor(QColor cl){pickCfgData.borderColor = cl;}
    QColor GetPickBorderColor(){return pickCfgData.borderColor;}

    void SetPickCrossColor(QColor cl){pickCfgData.crossColor = cl;}
    QColor GetPickCrossColor(){return pickCfgData.crossColor;}

    void SetPickRGBColor(QColor cl){pickCfgData.rgbColor;}
    QColor GetPickRGBColor(){return pickCfgData.rgbColor;}

    PickColorConfigData GetPickConfigData(){return pickCfgData;}

    QString GetDesktopPath();

private:
    static GlobalMgr* mIns;
    bool bInit;

    SnapConfigData snapCfgData;
    PickColorConfigData pickCfgData;
};

#endif // GLOBALMGR_H
