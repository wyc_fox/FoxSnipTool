# FoxSnipTool

#### 项目介绍
截图工具，主要用于桌面快速截图并图钉
初版用qt编写，后面转用c# 不在跨平台


#### 软件架构
output 文件夹下是打包好的工具，解压后直接使用
qt_version qt版源码
winform_version c#版源码


#### 使用说明

1. ctrl+1 进入截图模式 
2. ctrl+2 呼出取色工具,配合截图模式使用
3. ![设置界面](https://images.gitee.com/uploads/images/2021/0102/120641_ce84ee96_1123555.png "132540331151002154.png")
4. ![设置界面](https://images.gitee.com/uploads/images/2021/0102/120701_fd398f7a_1123555.png "132540331217001725.png")
5. ![设置界面](https://images.gitee.com/uploads/images/2021/0102/120714_83bdccad_1123555.png "132540331287791518.png")
6. ![截图](https://images.gitee.com/uploads/images/2021/0102/121509_57c93e43_1123555.gif "示例.gif")
7. ![取色](https://images.gitee.com/uploads/images/2021/0102/121531_b43902d8_1123555.gif "示例2.gif")
